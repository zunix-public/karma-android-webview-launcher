import {AdbCommandAsync} from '../src/AdbCommand/AdbCommandAsync';
import {ISpawnError} from '../src/AdbCommand/ISpawnError';

describe('Test person.ts', () => {
    let service: AdbCommandAsync;

    beforeEach(() => service = new AdbCommandAsync('device-42'));

    test('AdbDeviceId throught constructor', () => {
        expect(service.adbDeviceId ).toBe('device-42');
    });

    test('onProcessTerminated definition', (done) => {
        const callback = (code: number) => {
            expect(code).toBe(1); // Pas de device connecté
            done();
        };
        service.onProcessTerminated = callback;
        service.enableReverseTcp(42);
    });

    test('onProcessError definition', (done) => {
        (service as any)._adbBinary = 'nonexistant';
        const callback = (err: ISpawnError) => {
            try {
                expect(err.code).toBe('ENOENT');
                done();
            } catch (error) {
                done(error);
            }
        };
        service.onProcessError = callback;
        service.enableReverseTcp(42);
    });

    test('test enableReverseTcp', () => {
        const command = [
            'adb',
            '-s',
            'device-42',
            'reverse',
            'tcp:42',
            'tcp:42'
        ];
        service.enableReverseTcp(42);
        expect(service.lastAdbCommand).toStrictEqual(command);
    });

    test('test launchWebView', () => {
        const activityFullName = 'com.test/.ui';
        const url = 'http://toto.fr';
        const command = [
            'adb',
            '-s',
            'device-42',
            'shell',
            'am',
            'start',
            '-a',
            'android.intent.action.VIEW',
            '-n',
            activityFullName,
            '-d "' + url + '"'
        ];
        service.launchWebView(activityFullName, url);
        expect(service.lastAdbCommand).toStrictEqual(command);
    });

    test('test quitWebView', () => {
        const androidAppPackage = 'com.test';
        const command = [
            'adb',
            '-s',
            'device-42',
            'shell',
            'am',
            'force-stop',
            androidAppPackage
        ];
        service.quitWebView(androidAppPackage);
        expect(service.lastAdbCommand).toStrictEqual(command);
    });

    test('test onError', () => {
        const androidAppPackage = 'com.test';
        const command = [
            'adb',
            '-s',
            'device-42',
            'shell',
            'am',
            'force-stop',
            androidAppPackage
        ];
        service.quitWebView(androidAppPackage);
        expect(service.lastAdbCommand).toStrictEqual(command);
    });
});