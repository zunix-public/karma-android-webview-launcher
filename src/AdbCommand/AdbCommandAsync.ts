import { spawn } from 'child_process';
import { ISpawnError } from './ISpawnError';

/**
 * Construire et exécuter des commandes adb
 */
export class AdbCommandAsync {
    private readonly _adbBinary = 'adb';
    private readonly _adbDeviceId: string;
    private _lastAdbCommand: ReadonlyArray<string>;

    /**
     * Id device adb
     */
    public get adbDeviceId(): string {
        return this._adbDeviceId;
    }

    /**
     * Dernière commande adb envoyée
     */
    public get lastAdbCommand(): ReadonlyArray<string> {
        return this._lastAdbCommand;
    }

    /**
     * Action sur process terminé
     */
    public onProcessTerminated: (code: number) => void = null;

    /**
     * Action sur process en erreur
     */
    public onProcessError: (err: ISpawnError) => void = null;

    /**
     * Construire une commande adb
     * @param adbDeviceId id du device
     */
    constructor(adbDeviceId: string = null) {
        this._adbDeviceId = adbDeviceId;
    }

    /**
     * Activer le reverse forwarding de port tcp
     * @param tcpPort numéro de port (mirroir in/out)
     */
    public enableReverseTcp(tcpPort: number) {
        let adbArgs = this.buildArgs([
                'reverse',
                'tcp:' + tcpPort,
                'tcp:' + tcpPort
            ]);
        this.runAsync(adbArgs);
    }

    /**
     * Construire un tableau d'arguments ADB
     * @param args arguments a ajouter
     */
    private buildArgs(args: string[] ): string[] {
        const deviceIdArgs = [
            (this.adbDeviceId == null) ? null : '-s',
            (this.adbDeviceId == null) ? null : this.adbDeviceId
        ];
        return deviceIdArgs.concat(args).filter(n => n) as string[];
    }

    /**
     * Lancer la webview sur une URL
     * @param activityFullName nom complet de l'activité
     * @param url url de destination
     */
    public launchWebView(activityFullName: string,  url: string) {
        let adbArgs = this.buildArgs([
                'shell',
                'am',
                'start',
                '-a',
                'android.intent.action.VIEW',
                '-n',
                activityFullName,
                '-d "' + url + '"'
            ]);
        this.runAsync(adbArgs);
    }

    /**
     * Quitter la webview
     * @param androidAppPackage nom de package de la webview
     */
    public quitWebView(androidAppPackage: string) {
        let adbArgs = this.buildArgs([
            'shell',
            'am',
            'force-stop',
            androidAppPackage
        ]);
        this.runAsync(adbArgs);
    }

    /**
     * Exécuter une commande adn
     * @param adbArgs liste d'arguments adb
     */
    private runAsync(adbArgs: ReadonlyArray<string>) {
        const _process = spawn(this._adbBinary, adbArgs);
        _process.on('error', err => this.onError(err as unknown as ISpawnError));
        _process.on('close', code => this.onClose(code));
        this._lastAdbCommand = [this._adbBinary].concat(adbArgs);
    }

    /**
     * Action sur erreur
     * @param err erreur Spawn
     */
    private onError(err: ISpawnError) {
        if (this.onProcessError != null)
            this.onProcessError(err);
    }

    /**
     * Sur fermeture du process
     * @param code code d'erreur
     */
    private onClose(code: number) {
        if ( this.onProcessTerminated != null)
            this.onProcessTerminated(code);
    }
}