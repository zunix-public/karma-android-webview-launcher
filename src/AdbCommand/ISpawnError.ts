/**
 * Interface erreur conforme Spawn
 */
export interface ISpawnError {
    errno: String;
    code: String;
    syscall: String;
    path: String;
    spawnargs: Array<String>;
}