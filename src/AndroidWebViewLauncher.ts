import {ISpawnError} from './AdbCommand/ISpawnError';
import {AdbCommandAsync} from './AdbCommand/AdbCommandAsync';

/**
 * Implémentation launcher Android Webview
 */
export class AndroidWebViewLauncher {

    //region Statics

    public static $inject = ['args', 'logger'];
    private static readonly PLUGIN_NAME = 'AndroidWebviewLauncher';

    //endregion

    //region Properties

    private id: any;
    private log: any;
    private name: any;
    private adbDeviceId: string;
    private androidComponent: string;

    //endregion

   //region Constructor

   constructor(args, logger) {
       let launcherName = AndroidWebViewLauncher.PLUGIN_NAME;
       this.name = launcherName;
       this.log = logger.create(launcherName);
       this.adbDeviceId = args.adbDeviceId || null;
       this.androidComponent = args.androidComponent || null;
    }

    //endregion

    public start(pageUrl: string) {
            try {
                this.log.debug('AndroidWebView : start()');
                this.checkPluginOptions();
                this.enableReverseTcpForLocalhost(pageUrl);
                this.launchWebView(pageUrl);
            } catch (error) {
                this.onError(error);
            }
    }

    /**
     * Vérifier la validité des arguments
     */
    private checkPluginOptions = function() {
        if (this.isNullOrEmpty(this.androidComponent)) {
            this.log.error('Package Android null ou vide');
            throw 'Package Android null ou vide';
        }
    };

    /**
     * Vérifier si une chaine est nulle ou vide
     * @param value
     */
    private isNullOrEmpty = function(value) {
        return (value == null || value === '');
    };

    /**
     * Activer le port forwarding adb sur le terminal
     * @param url
     */
    private enableReverseTcpForLocalhost = function(url) {
        const testUrl = new URL(url);
        if (!this.isNullOrEmpty(testUrl.port) && testUrl.hostname === 'localhost') {
            this.log.debug('Activation mode port forwarding localhost du port %s', testUrl.port);
            let adbCommand = new AdbCommandAsync(this.adbDeviceId);
            adbCommand.enableReverseTcp(Number(testUrl.port));
        }
    };

    /**
     * Démarrer le navigateur sur le terminal
     * @param url
     */
    private launchWebView = function(url) {
        const urlKarma = url + '?id=' + this.id;
        this.log.debug('Démarrage de %s sur %s id[%s] avec %s', this.androidComponent , this.name, this.id, this.adbCommand);
        let adbCommand = new AdbCommandAsync(this.adbDeviceId);
        adbCommand.launchWebView(this.androidComponent, urlKarma);
    };

    /**
     * Action sur erreur
     * @param error error
     */
    public onError = function(error: any) {
        this.log.error('Error : ' + error);
    };

    /**
     * Surcharge Karma
     */
    public forceKill = function() {
        return this.kill();
    };

    /**
     * Surcharge Karma
     */
    public kill = function(doneFn: () => void) {
        try {
            let androidComponent = this.getPackageName();
            this.log.debug('AndroidWebView : kill() for ' + androidComponent);

            return new Promise((resolve, reject) => {
                   let adbCommand = new AdbCommandAsync();
                   adbCommand.onProcessTerminated = (code: number) => resolve({message: "success", error: null});
                   adbCommand.onProcessError = (err: ISpawnError) => resolve({message: "error", error: err.errno});
                   adbCommand.quitWebView(androidComponent);
              });

            if (doneFn)
                doneFn();
        } catch (error) {
            this.onError(error);
        }
    };

    /**
    * Obtenir nom package a partir d'un nom de composant complet
    * @return nom package
    */
    private getPackageName() {
        return this.androidComponent.split('/')[0];
    }

    /**
     * Surcharge Karma
     */
    public restart = function() {
    };

    /**
     * Surcharge Karma
     */
    public markCaptured = function() {
    };

    /**
     * Surcharge Karma
     */
    public isCaptured = function() {
        return true;
    };

    /**
     * Surcharge Karma
     */
    public on(actionName, action: (input: string) => void) {
        this.log.debug('on(actionName) actionName : ' + actionName);
    }
}