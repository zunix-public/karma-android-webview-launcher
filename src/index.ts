import {AndroidWebViewLauncher} from './AndroidWebViewLauncher';

/**
 * Déclaration du module
 */
module.exports = {
    'launcher:AndroidWebView': ['type', AndroidWebViewLauncher],
};