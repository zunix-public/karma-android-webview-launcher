"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var AdbCommandAsync_1 = require("./AdbCommand/AdbCommandAsync");
/**
 * Implémentation launcher Android Webview
 */
var AndroidWebViewLauncher = /** @class */ (function () {
    //endregion
    //region Constructor
    function AndroidWebViewLauncher(args, logger) {
        /**
         * Vérifier la validité des arguments
         */
        this.checkPluginOptions = function () {
            if (this.isNullOrEmpty(this.androidComponent)) {
                this.log.error('Package Android null ou vide');
                throw 'Package Android null ou vide';
            }
        };
        /**
         * Vérifier si une chaine est nulle ou vide
         * @param value
         */
        this.isNullOrEmpty = function (value) {
            return (value == null || value === '');
        };
        /**
         * Activer le port forwarding adb sur le terminal
         * @param url
         */
        this.enableReverseTcpForLocalhost = function (url) {
            var testUrl = new URL(url);
            if (!this.isNullOrEmpty(testUrl.port) && testUrl.hostname === 'localhost') {
                this.log.debug('Activation mode port forwarding localhost du port %s', testUrl.port);
                var adbCommand = new AdbCommandAsync_1.AdbCommandAsync(this.adbDeviceId);
                adbCommand.enableReverseTcp(Number(testUrl.port));
            }
        };
        /**
         * Démarrer le navigateur sur le terminal
         * @param url
         */
        this.launchWebView = function (url) {
            var urlKarma = url + '?id=' + this.id;
            this.log.debug('Démarrage de %s sur %s id[%s] avec %s', this.androidComponent, this.name, this.id, this.adbCommand);
            var adbCommand = new AdbCommandAsync_1.AdbCommandAsync(this.adbDeviceId);
            adbCommand.launchWebView(this.androidComponent, urlKarma);
        };
        /**
         * Action sur erreur
         * @param error error
         */
        this.onError = function (error) {
            this.log.error('Error : ' + error);
        };
        /**
         * Surcharge Karma
         */
        this.forceKill = function () {
            return this.kill();
        };
        /**
         * Surcharge Karma
         */
        this.kill = function (doneFn) {
            try {
                var androidComponent_1 = this.getPackageName();
                this.log.debug('AndroidWebView : kill() for ' + androidComponent_1);
                return new Promise(function (resolve, reject) {
                    var adbCommand = new AdbCommandAsync_1.AdbCommandAsync();
                    adbCommand.onProcessTerminated = function (code) { return resolve({ message: "success", error: null }); };
                    adbCommand.onProcessError = function (err) { return resolve({ message: "error", error: err.errno }); };
                    adbCommand.quitWebView(androidComponent_1);
                });
                if (doneFn)
                    doneFn();
            }
            catch (error) {
                this.onError(error);
            }
        };
        /**
         * Surcharge Karma
         */
        this.restart = function () {
        };
        /**
         * Surcharge Karma
         */
        this.markCaptured = function () {
        };
        /**
         * Surcharge Karma
         */
        this.isCaptured = function () {
            return true;
        };
        var launcherName = AndroidWebViewLauncher.PLUGIN_NAME;
        this.name = launcherName;
        this.log = logger.create(launcherName);
        this.adbDeviceId = args.adbDeviceId || null;
        this.androidComponent = args.androidComponent || null;
    }
    //endregion
    AndroidWebViewLauncher.prototype.start = function (pageUrl) {
        try {
            this.log.debug('AndroidWebView : start()');
            this.checkPluginOptions();
            this.enableReverseTcpForLocalhost(pageUrl);
            this.launchWebView(pageUrl);
        }
        catch (error) {
            this.onError(error);
        }
    };
    /**
    * Obtenir nom package a partir d'un nom de composant complet
    * @return nom package
    */
    AndroidWebViewLauncher.prototype.getPackageName = function () {
        return this.androidComponent.split('/')[0];
    };
    /**
     * Surcharge Karma
     */
    AndroidWebViewLauncher.prototype.on = function (actionName, action) {
        this.log.debug('on(actionName) actionName : ' + actionName);
    };
    //region Statics
    AndroidWebViewLauncher.$inject = ['args', 'logger'];
    AndroidWebViewLauncher.PLUGIN_NAME = 'AndroidWebviewLauncher';
    return AndroidWebViewLauncher;
}());
exports.AndroidWebViewLauncher = AndroidWebViewLauncher;
//# sourceMappingURL=AndroidWebViewLauncher.js.map