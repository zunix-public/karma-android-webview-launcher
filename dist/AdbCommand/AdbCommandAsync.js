"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var child_process_1 = require("child_process");
/**
 * Construire et exécuter des commandes adb
 */
var AdbCommandAsync = /** @class */ (function () {
    /**
     * Construire une commande adb
     * @param adbDeviceId id du device
     */
    function AdbCommandAsync(adbDeviceId) {
        if (adbDeviceId === void 0) { adbDeviceId = null; }
        this._adbBinary = 'adb';
        /**
         * Action sur process terminé
         */
        this.onProcessTerminated = null;
        /**
         * Action sur process en erreur
         */
        this.onProcessError = null;
        this._adbDeviceId = adbDeviceId;
    }
    Object.defineProperty(AdbCommandAsync.prototype, "adbDeviceId", {
        /**
         * Id device adb
         */
        get: function () {
            return this._adbDeviceId;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AdbCommandAsync.prototype, "lastAdbCommand", {
        /**
         * Dernière commande adb envoyée
         */
        get: function () {
            return this._lastAdbCommand;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * Activer le reverse forwarding de port tcp
     * @param tcpPort numéro de port (mirroir in/out)
     */
    AdbCommandAsync.prototype.enableReverseTcp = function (tcpPort) {
        var adbArgs = this.buildArgs([
            'reverse',
            'tcp:' + tcpPort,
            'tcp:' + tcpPort
        ]);
        this.runAsync(adbArgs);
    };
    /**
     * Construire un tableau d'arguments ADB
     * @param args arguments a ajouter
     */
    AdbCommandAsync.prototype.buildArgs = function (args) {
        var deviceIdArgs = [
            (this.adbDeviceId == null) ? null : '-s',
            (this.adbDeviceId == null) ? null : this.adbDeviceId
        ];
        return deviceIdArgs.concat(args).filter(function (n) { return n; });
    };
    /**
     * Lancer la webview sur une URL
     * @param activityFullName nom complet de l'activité
     * @param url url de destination
     */
    AdbCommandAsync.prototype.launchWebView = function (activityFullName, url) {
        var adbArgs = this.buildArgs([
            'shell',
            'am',
            'start',
            '-a',
            'android.intent.action.VIEW',
            '-n',
            activityFullName,
            '-d "' + url + '"'
        ]);
        this.runAsync(adbArgs);
    };
    /**
     * Quitter la webview
     * @param androidAppPackage nom de package de la webview
     */
    AdbCommandAsync.prototype.quitWebView = function (androidAppPackage) {
        var adbArgs = this.buildArgs([
            'shell',
            'am',
            'force-stop',
            androidAppPackage
        ]);
        this.runAsync(adbArgs);
    };
    /**
     * Exécuter une commande adn
     * @param adbArgs liste d'arguments adb
     */
    AdbCommandAsync.prototype.runAsync = function (adbArgs) {
        var _this = this;
        var _process = child_process_1.spawn(this._adbBinary, adbArgs);
        _process.on('error', function (err) { return _this.onError(err); });
        _process.on('close', function (code) { return _this.onClose(code); });
        this._lastAdbCommand = [this._adbBinary].concat(adbArgs);
    };
    /**
     * Action sur erreur
     * @param err erreur Spawn
     */
    AdbCommandAsync.prototype.onError = function (err) {
        if (this.onProcessError != null)
            this.onProcessError(err);
    };
    /**
     * Sur fermeture du process
     * @param code code d'erreur
     */
    AdbCommandAsync.prototype.onClose = function (code) {
        if (this.onProcessTerminated != null)
            this.onProcessTerminated(code);
    };
    return AdbCommandAsync;
}());
exports.AdbCommandAsync = AdbCommandAsync;
//# sourceMappingURL=AdbCommandAsync.js.map