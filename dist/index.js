"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var AndroidWebViewLauncher_1 = require("./AndroidWebViewLauncher");
/**
 * Déclaration du module
 */
module.exports = {
    'launcher:AndroidWebView': ['type', AndroidWebViewLauncher_1.AndroidWebViewLauncher],
};
//# sourceMappingURL=index.js.map