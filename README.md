# karma-android-webview-launcher

> Run Karma tests on any adb connected android webview application.\
> Reverse port is mapped on device if neeeded : tests can be run through your local developement IDE using Karma urls like http://0.0.0.0:9876 at runtime. \
> Note : adb must be registered on PATH to communicate with connected device or emulator.


## Installation

Dependency `karma-android-webview-launcher` must be declared in your dependency manager. Kotlin JS build.gradle exemple :
```gradle
dependencies {
    ...
    // TU Karma
    testImplementation "org.jetbrains.kotlin:kotlin-test-js"
    testImplementation api(npm("puppeteer", "*")) // Chrome headless
    testImplementation npm('karma-android-webview-launcher','^1.1.0')
}
```

## Project side (karma.conf.js)
```js
// local karma.conf.js
config.set({
    logLevel: config.LOG_DEBUG,
    browsers: ['ChromeHeadless', 'AppNavigateurWeb'],
    customLaunchers: {
      AppNavigateurWeb: {
        base: 'AndroidWebView',
        androidComponent: 'com.test.app/.ui.MainActivity'
        //, adbDeviceId: 'emulator-5554' /* -> can be ommited to use connected device */
      }
    }
});
```


## Android app side
```kotlin
// In functions
    override fun onCreate(savedInstanceState: Bundle?)
    override fun onNewIntent(intent: Intent?)

// use URI in intent data
   val uri = intent?.data 

// to redirect webview to karma test page
mywebview.load(uri)
```

Have fun, and thanks to Karma team !